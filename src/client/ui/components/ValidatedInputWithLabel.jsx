import React from "react"
import TextInput from "./TextInput"




const TextInputWithLabel = ({
    label,
    maxLength=30,
    name,
    placeholder,
    validator,
}) => {
    return <div className="input-with-label">
        <label htmlFor="name">{label}: </label>
        <TextInput
            placeholder={placeholder}
            maxLength={maxLength}
            name={name}
            validator={validator}
        />
    </div>
}




export default TextInputWithLabel
