/**
 * ## Akelius FV.
 *
 * Methods in this module service Redux state persistence.
 * Current configuration persists state in `sessionStorage`.
 *
 * @module state-persistence
 * @license Apache-2.0
 * @author Igor Wasiński
 */




import {
    handleException,
    nullToUndefined,
} from "@xcmats/js-toolbox"
import { ssAppStateKey } from "../config/frontend"




/**
 * Restore current Redux state from `sessionStorage`.
 *
 * @function loadState
 * @returns {any}
 */
export const loadState = () =>
    handleException(
        () => nullToUndefined(
            JSON.parse(sessionStorage.getItem(ssAppStateKey))
        ),
        // eslint-disable-next-line no-console
        (ex) => nullToUndefined(console.log(ex))
    )




/**
 * Persist current state of the application.
 *
 * @function saveState
 * @param {Object} state Current _Redux_ state snapshot.
 * @returns {any}
 */
export const saveState = (state) =>
    handleException(
        () => sessionStorage.setItem(ssAppStateKey, JSON.stringify(state)),
        // eslint-disable-next-line no-console
        console.log
    )
