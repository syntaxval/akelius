/**
 * ## Akelius FV.
 *
 * Client application (frontend-logic-controller). It is responsible
 * for orchestrating the very first UI exposed to the user. Under
 * the hood it lazy loads some needed libraries and runs callbacks
 * once things finish loading.
 *
 * @module client-app
 * @license Apache-2.0
 * @author Igor Wasiński
 */




import { run } from "@xcmats/js-toolbox"
import { consoleWrapper } from "./lib/utils"
import * as serviceWorker from "./serviceWorker"
import "./index.css"




/**
 * @async
 * @function importLibs
 * @returns {Object}
 */
const importLibs = async () => ({
    redux: await import("redux"),
    reactRedux: await import("react-redux"),
})




/**
 * Run the following function on load of document.
 */
run(async () => {

    const
        // safe console logger
        logger = consoleWrapper("⚛️")




    // hide main loading spinner the moment custom fonts have loaded
    document.fonts.ready.then(
        setTimeout(() => document
            .getElementById("pre-flight-loader")
            .className = "pre-flight-loader-off", 300
        )
    )



    // just a console greeting
    logger.info("Boom! 💥")




    // lazy load libraries
    await importLibs()




    // execute main UI controller
    await import("./client/ui/main")

})


// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister()
