import {
    FORM__SET_VALID,
    UI__SET_VIEWPORT_DIMENSIONS,
    UI__SET_PROGRESS_PERCENTAGE,
    UI__SET_PROGRESS_STATUS,
    UI__SET_PROGRESS_TEXT,
} from "../../actionTypes"
import { string } from "@xcmats/js-toolbox"
import { progressStatus } from "../../../../lib/constants"




// ...
let initialState = {
    viewport: {
        width: null,
        height: null,
    },
    progress: {
        percent: null,
        status: progressStatus.loading,
        text: string.empty(),
    },
    form: {
        valid: false,
    },
}




// ...
export const viewport = (prevState = initialState.viewport, action) => {
    switch (action.type) {
        case UI__SET_VIEWPORT_DIMENSIONS:
            return Object.assign({}, prevState, {
                ...action.dimensions,
            })
        default:
            return prevState
    }
}




// ...
export const progress = (prevState = initialState.progress, action)  => {
    switch (action.type) {
        case UI__SET_PROGRESS_PERCENTAGE:
            return Object.assign({}, prevState, {
                percent: action.percent,
            })
        case UI__SET_PROGRESS_TEXT:
            return Object.assign({}, prevState, {
                text: action.text,
            })
        case UI__SET_PROGRESS_STATUS:
            return Object.assign({}, prevState, {
                status: action.status,
            })
        default:
            return prevState
    }
}




// ...
export const form = (prevState = initialState.form, action) => {
    switch (action.type) {
        case FORM__SET_VALID:
            return Object.assign({}, prevState, {
                valid: action.valid,
            })
        default:
            return prevState
    }
}
