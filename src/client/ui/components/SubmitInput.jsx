import React, { Component } from "react"




// ...
class SubmitInput extends Component {


    // ...
    render () {
        return <input
            name={this.props.name}
            className="input-submit"
            type="submit"
            value="Submit"
            id="submit"
        />
    }


}




// ...
export default SubmitInput
