/**
 * ## Akelius FV.
 *
 * Isomorphic various utilities.
 *
 * @module utils-lib
 * @license Apache-2.0
 * @author Igor Wasiński
 */




import {
    func,
    string,
    struct,
    utils,
} from "@xcmats/js-toolbox"




/**
 * Take console-like object and "augment" all its methods in a way
 * that all arguments passed to them are also passed to function given
 * as an argument.
 *
 * @function consoleAugmenter
 * @param {Object} con console-like object
 * @param {Function} f
 * @returns {Object}
 */
export const consoleAugmenter = (() => {
    let
        methods = ["log", "info", "warn", "error"],
        c = (con, f) => struct.dict(methods.map(
            (m) => [m, (...args) => { f(m, ...args); con[m](...args) }]
        ))
    return c
})()




/**
 * Safe version of (window/self).console object.
 *
 * @function consoleWrapper
 * @param {String} context
 * @returns {Object}
 */
export const consoleWrapper = (() => {
    let
        methods = ["log", "info", "warn", "error"],
        noop = struct.dict(methods.map((m) => [m, () => null])),
        c = (context = "main") => (
            (con) => struct.dict(methods.map(
                (m) => [m, func.partial(con[m])(string.quote(context, "[]"))]
            ))
        )(struct.access(utils.isBrowser() ? window : global, ["console"], noop))
    c.noop = noop
    return c
})()




/**
 * Get current viewport size from _window_ object.
 *
 * @function getViewportSize
 * @returns {Object}
 */
export const getViewportSize = () =>
    window ? {
        width: window.innerWidth,
        height: window.innerHeight,
    } : {
        width: 0,
        height: 0,
    }
