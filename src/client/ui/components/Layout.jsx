import React, { lazy, Suspense } from "react"
import LazyLoader from "./LazyLoader"


const MainForm = lazy(() => import("./MainForm"))



const Layout = () => {
    return <Suspense fallback={<LazyLoader />}>
        <MainForm />
    </Suspense>
}




// ...
export default Layout
