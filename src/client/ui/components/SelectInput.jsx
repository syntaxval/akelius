import React, { Component } from "react"




// ...
class SelectInput extends Component {




    // ...
    constructor (props) {

        super(props)

        this.state = {
            inputValue: "",
        }

        this.handleChange = this.handleChange.bind(this)

    }




    // Prevent unnecessary reconciliation.
    shouldComponentUpdate (_nextProps, _nextState) {
        return false
    }




    // ...
    handleChange ({target: {value}}) {
        this.setState({
            inputValue: value,
        })
    }




    // ...
    render () {
        return <select
            aria-required="true"
            name={this.props.name}
            required
            pattern={this.props.validator}
            onChange={this.handleChange}
        >
            <option value="">Please Select</option>
            {this.props.data && this.props.data.map((d) => {
                return Object.keys(d).map(
                    (key) => <option value={key} key={key}>{d[key]}</option>
                )
            })}
        </select>
    }




}




// ...
export default SelectInput







// import React from "react"
// import classnames from "classnames"



// // const isValid = function (value, validator) {
// //     return validator.test(value)
// // }




// const SelectInput = ({ valid, setValid, setValue, validator, label, data }) => {

//     const validateInput = function (event) {
//         setValue(event.target.value)
//         // setValid(isValid(event.target.value, validator))
//     }

//     return <div className="input-with-label">
//         <label htmlFor="name">{label}: </label>
//         <select
//             className={classnames(!valid && "select-invalid")}
//             required
//             onChange={validateInput}
//         >
//             <option value="">Please Select</option>
//             {data && data.map((d) => {
//                 return Object.keys(d).map(
//                     (key) => <option value={key} key={key}>{d[key]}</option>
//                 )
//             })}
//         </select>
//     </div>
// }




// export default SelectInput
