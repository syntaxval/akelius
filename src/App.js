import React from "react"
import logo from "./logo.svg"
import "./App.css"

function App () {
    return (
        <div className="App">
            <header className="App-header">
                <img src={logo} className="App-logo" alt="logo" />
                <section>
                  Redux Dispatcher / Listener
                </section>
                <section>
                    <p>
                      Viewport: W:<span id="viewport-width"></span> H:<span id="viewport-height"></span>
                    </p>
                    <p>Loading: <span id="currently-loading"></span></p>
                </section>
            </header>
        </div>
    )
}

export default App
