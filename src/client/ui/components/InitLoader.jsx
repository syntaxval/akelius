import React from "react"
import { bindActionCreators } from "redux"
import { connect } from "react-redux"
import "./InitLoader.css"



// ...
const InitLoader = ({ percent }) => {
    return <main className="init-loader-on">
        Loading ...{percent} %
    </main>
}




// ...
export default connect(
    (state) => ({
        height: state.viewport.height,
        width: state.viewport.width,
        percent: state.progress.percent,
    }),
    (dispatch) => bindActionCreators({

    }, dispatch),
)(InitLoader)
