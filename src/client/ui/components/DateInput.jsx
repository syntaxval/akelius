import React, { Component } from "react"




// ...
class TextInput extends Component {


    // ...
    constructor (props) {

        super(props)

        this.state = {
            inputValue: "",
            max: new Date().toISOString().split("T")[0],
            min: this.props.min,
        }

        this.handleChange = this.handleChange.bind(this)

    }




    // Prevent unnecessary reconciliation.
    shouldComponentUpdate (_nextProps, _nextState) {
        return false
    }




    // ...
    handleChange ({target: {value}}) {
        this.setState({
            inputValue: value,
        })

        // update `min` attribute for the component in question
        // (without using refs)
        if (this.props.updatesMinFor) {
            let datePicker = document.getElementById(this.props.updatesMinFor)
            datePicker.setAttribute("min", value)
        }
    }




    // ...
    render () {
        return <input
            aria-required="true"
            type="date"
            id={this.props.id}
            name="form-input"
            min={this.state.min}
            max={this.state.max}
            required
            onChange={this.handleChange}
        />
    }


}




// ...
export default TextInput
