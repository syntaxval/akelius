import {
    UI__SET_PROGRESS_PERCENTAGE,
    UI__SET_PROGRESS_STATUS,
    UI__SET_PROGRESS_TEXT,
    UI__SET_VIEWPORT_DIMENSIONS,
    UI__SET_VIEWPORT_HEIGHT,
    UI__SET_VIEWPORT_WIDTH,
    FORM__SET_VALID,
} from "./actionTypes"




const actionCreatorFactory = (type, ...argNames) =>
    (...args) => {
        const action = { type }
        argNames.forEach((_arg, index) => {
            action[argNames[index]] = args[index]
        })
        return action
    }




export const setViewportDimensions = actionCreatorFactory(
    UI__SET_VIEWPORT_DIMENSIONS,
    "dimensions",
)




export const setViewportHeight = actionCreatorFactory(
    UI__SET_VIEWPORT_HEIGHT,
    "height"
)




export const setViewportWidth = actionCreatorFactory(
    UI__SET_VIEWPORT_WIDTH,
    "width"
)




export const setProgressPercentage = actionCreatorFactory(
    UI__SET_PROGRESS_PERCENTAGE,
    "percent"
)




export const setProgressText = actionCreatorFactory(
    UI__SET_PROGRESS_TEXT,
    "text"
)




export const setProgressStatus = actionCreatorFactory(
    UI__SET_PROGRESS_STATUS,
    "status"
)




export const setFormValid = actionCreatorFactory(
    FORM__SET_VALID,
    "valid"
)
