import React from "react"




// ...
const Results = ({ data }) => {
    return <div className="results">
        <p>Submitted Results:</p>
        <section>
            <ul>
                {Object.keys(data).map(
                    (key) => <li key={key}>{data[key]}</li>
                )}
            </ul>
        </section>
    </div>
}




// ...
export default Results
