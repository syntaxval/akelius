import React from "react"
import PropTypes from "prop-types"
import {
    connect,
    Provider,
} from "react-redux"
import Layout from "./Layout"
import InitLoader from "./InitLoader"
import { progressStatus } from "../../../lib/constants"




const Ui = ({ status, store }) =>
    <Provider store={store}>
        {status === progressStatus.loading ?
            <InitLoader /> :
            <Layout />
        }
    </Provider>




Ui.propTypes = {
    store: PropTypes.object.isRequired,
}




// ...
export default connect(
    (state) => ({
        status: state.progress.status,
    })
)(Ui)
