import React, { Component } from "react"
import ValidatedDateWithLabel from "./ValidatedDateWithLabel"
import ValidatedInputWithLabel from "./ValidatedInputWithLabel"
import ValidatedSelectWithLabel from "./ValidatedSelectWithLabel"
import SubmitInput from "./SubmitInput"
import Results from "./Results"



const countryList = [
    {AF: "Afghanistan"},
    {BE: "Belgium"},
    {US: "United States of America and American Samoa"},
]

const sexList = [
    {M: "Male"},
    {F: "Female"},
    {U: "Unspecified"},
]

const nationalityList = [
    {ALB: "Albanian"},
    {DKK: "Danish"},
]

const nameValidator = "^[A-Z][a-zA-Z\\s][^#&<>\"~;$^%{}?]{1,20}$"
const passportNumberValidator = "^(?!^0+$)[a-zA-Z0-9]{5,15}$"
const countryValidator = "^[A-Z]{2}$"
const sexValidator = "^[m|f|d]{1,}$"
const nationalityValidator = "^[A-Z]{3}$"











class MainForm extends Component {


    constructor (props) {
        super(props)

        this.state = {
            showResults: false,
            results: [],
        }

        this.handleKeyUp = this.handleKeyUp.bind(this)
        this.handleSubmit = this.handleSubmit.bind(this)

        window.addEventListener("keyup", this.handleKeyUp)
    }


    handleKeyUp (event) {
        event.key === "Enter" && document.getElementById("submit").click()
    }


    handleSubmit (event) {
        event.preventDefault()
        const inputNodes = document.getElementsByName("form-input")
        this.setState({
            showResults: true,
            results: [...inputNodes].map((inputNode) => inputNode.value),
        })
        return false
    }



    render () {
        return <main>
            <form onSubmit={this.handleSubmit} className="main-form">

                <ValidatedInputWithLabel
                    ariaRequired={true}
                    maxLength={30}
                    name="form-input"
                    label="Name"
                    validator={nameValidator}
                    placeholder="First Last"
                />

                <ValidatedInputWithLabel
                    ariaRequired={true}
                    maxLength={15}
                    name="form-input"
                    label="Passport Number"
                    validator={passportNumberValidator}
                    placeholder="X[XX]123456789"
                    required={true}
                />

                <ValidatedSelectWithLabel
                    ariaRequired={true}
                    name="form-input"
                    label={"Country"}
                    data={countryList}
                    validator={countryValidator}
                    required={true}
                />

                <ValidatedSelectWithLabel
                    name="form-input"
                    label={"Sex"}
                    data={sexList}
                    validator={sexValidator}
                />

                <ValidatedSelectWithLabel
                    name="form-input"
                    label={"Nationality"}
                    data={nationalityList}
                    validator={nationalityValidator}
                />

                <ValidatedDateWithLabel
                    name="form-input"
                    label={"DOB"}
                    updatesMinFor="passportExpiryDate"
                />

                <ValidatedDateWithLabel
                    id="passportExpiryDate"
                    name="form-input"
                    label={"Passport Expiry"}
                />

                <SubmitInput />

            </form>

            {this.state.showResults && <Results data={this.state.results} />}
        </main>
    }


}




// ...
export default MainForm
