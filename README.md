In the project directory, you can run:

### `npm i`

and then

### `npm start`

Runs the app in the development mode.<br>
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br>
You will also see any lint errors in the console.

### `npm run build`

Builds the app for production to the `build` folder.<br>
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.<br>
Your app is ready to be deployed!

You can also run a static file server to server your build directory locally.

### `python -m SimpleHTTPServer`

Runs the app in the production mode.<br>
Open [http://localhost:8000](http://localhost:8000) to view it in the browser.
