import React from "react"
import DateInput from "./DateInput"



const DateInputWithLabel = ({ id, label, updatesMinFor, min="1901-01-01", name }) => {

    return <div className="input-with-label">
        <label htmlFor="name">{label}: </label>
        <DateInput updatesMinFor={updatesMinFor} id={id} min={min} name={name} />
    </div>
}




export default DateInputWithLabel
