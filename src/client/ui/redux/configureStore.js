/**
 * ## Akelius FV.
 *
 * Custom _Redux Store_ configuration and creation.
 *
 * @module store-config
 * @license Apache-2.0
 */




import {
    applyMiddleware,
    compose,
    combineReducers,
    createStore,
} from "redux"
import thunkMiddleware from "redux-thunk"
import { composeWithDevTools } from "redux-devtools-extension"
import rootReducer from "./reducers"
// import { monitorReducerEnhancer } from "./enhancers/monitorReducers"
import { devEnv } from "@xcmats/js-toolbox"




/**
 * Gather all logic of store creation in one place.
 * @function configureStore
 * @param {Object} preloadedState
 * @returns {Object} store
 */
export const configureStore = (preloadedState) => {
    const middlewares = [thunkMiddleware]
    const middlewareEnhancer = applyMiddleware(...middlewares)
    const enhancers = [middlewareEnhancer /*, monitorReducerEnhancer */]
    const composedEnhancers = devEnv ?
        composeWithDevTools(...enhancers) : compose(...enhancers)

    return createStore(
        combineReducers(
            rootReducer
        ),
        preloadedState,
        composedEnhancers
    )
}
