import React, { Component } from "react"




// ...
class TextInput extends Component {




    // ...
    constructor (props) {

        super(props)

        this.state = {
            inputValue: "",
        }

        this.handleChange = this.handleChange.bind(this)

    }




    // Prevent unnecessary reconciliation.
    shouldComponentUpdate (_nextProps, _nextState) {
        return false
    }




    // ...
    handleChange ({target: {value}}) {
        this.setState({
            inputValue: value,
        })
    }




    // ...
    render () {
        return <input
            aria-required="true"
            placeholder={this.props.placeholder}
            maxLength={this.props.maxLength}
            name={this.props.name}
            onChange={this.handleChange}
            type="text"
            required
            pattern={this.props.validator}
        />
    }




}




// ...
export default TextInput
