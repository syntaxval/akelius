import React from "react"
import SelectInput from "./SelectInput"



const ValidatedSelectWithLabel = ({ data, label, name, validator }) => {
    return <div className="input-with-label">
        <label htmlFor="name">{label}: </label>
        <SelectInput data={data} name={name} validator={validator} />
    </div>
}




export default ValidatedSelectWithLabel
