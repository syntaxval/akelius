/**
 * Akelius FV.
 *
 * Thunks.
 *
 * @module thunks-ui
 * @license Apache-2.0
 * @author Igor Wasiński
 */




import { progressStatus } from "../../../../lib/constants"
import {
    setFormValid,
    setProgressPercentage,
    setProgressStatus,
} from "../../actionCreators"




/**
 * @function loadLibraries
 * @returns {Function} thunk action
 */
export const loadLibraries = () =>
    async (dispatch, _getState) => {

        await dispatch(await setProgressPercentage(100))
        await dispatch(await setProgressStatus(progressStatus.loaded))
    }




export const setFormValidityState = (formIsValid) =>
    async (dispatch, _getState) => {
        await dispatch(await setFormValid(formIsValid))
    }
