// User Interface entry point.

import Root from "./components/Root"
import React from "react"
import ReactDOM from "react-dom"
import { getViewportSize } from "../../lib/utils"
import { configureStore } from "./redux/configureStore"
import {
    setProgressPercentage,
    setProgressStatus,
    setViewportDimensions,
} from "./actionCreators"
import {
    devEnv,
    isObject,
} from "@xcmats/js-toolbox"
import throttle from "lodash.throttle"
import {
    loadState,
    saveState,
} from "../../lib/state.persistence"
import { progressStatus } from "../../lib/constants"



const store = configureStore(loadState())




const updateViewportSize = () => store.dispatch(setViewportDimensions(getViewportSize()))




window.addEventListener("resize", updateViewportSize)




if (devEnv()) {
    if (!isObject(window.N)) window.N = {}
    window.N = {
        ...window.N,
        store,
    }
}




// save state in session storage
store.subscribe(throttle(
    () => saveState(store.getState())
))



store.dispatch(setProgressPercentage(0))
store.dispatch(setProgressStatus(progressStatus.loading))
store.dispatch(setViewportDimensions(getViewportSize()))

// lazy-load substantially large libraries here and report progress

store.dispatch(setProgressPercentage(100))
store.dispatch(setProgressStatus(progressStatus.loaded))


ReactDOM.render(
    <Root store={store} />,
    document.getElementById("root")
)
